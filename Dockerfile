FROM golang

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64
COPY . /snippet
WORKDIR /snippet
RUN go mod download
RUN chmod +x /snippet/cmd/web
RUN go build /snippet/cmd/web
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /snippet .
EXPOSE 4000
ENTRYPOINT ./web